import tkinter
import os.path
from tkinter import *

class PresentationTop (Frame):
	def __init__(self, controller, topPresentation):
		super().__init__(topPresentation)
		self.topPresentation = topPresentation
		self.controller = controller
		
		
		#Creation du menu
		monMenu = Menu(self.master)
		self.master.config(menu=monMenu)
		fichier = Menu(monMenu, tearoff=0)
		fichier.add_command(label="Sauvegarder",command=self.save_file)
		fichier.add_command(label="Charger",command=self.load_file)
		fichier.add_command(label="Exporter",command=self.export_file)
		fichier.add_separator()
		fichier.add_command(label="Quitter", command=self.master.destroy)
		monMenu.add_cascade(label="Fichier",menu=fichier)
		
		
		self.volumeGlobalSlider = Scale(self , orient=VERTICAL, command = self.modifyVolumeGlobalValue )
		self.volumeGlobalSlider.config(from_ = 100, to = 0)
		self.volumeGlobalSlider.pack()
		
		soundOff=tkinter.PhotoImage(file=os.path.join(os.path.abspath(os.path.dirname("..\Images\pause.png")), "pause.png"),master=self.topPresentation)
		self.soundButton = Button(self,image=soundOff, command = self.clickedSoundButton )
		self.soundButton.image = soundOff
		self.soundButtonState = False
		self.soundButton.pack()
		
		self.addChannel = Button(self,text= "Add channel", command=self.controller.addPiste)
		self.addChannel.pack()
		
		self.saveButton = Button(self,text= "Sauvegarder les paramètres",command=self.save_file)
		self.saveButton.pack()
		
		self.loadButton = Button(self,text= "Charger des paramètres",command=self.load_file)
		self.loadButton.pack()
		
		self.exportButton = Button(self,text = "Exporter",command=self.export_file)
		self.exportButton.pack()
		
		self.configure(background="#FFF")
		self.pack(side = LEFT)
		
	"""
	----------------------------Signal-----------------------------------------
	"""
	
	"""
		Méthode qui signale le changement du volume global
	"""
	def modifyVolumeGlobalValue(self,val):
		self.controller.listenerVolumeGeneral(int(val))
		
	"""
		Méthode qui signale le début d'une sauvegarde
	"""
	def save_file(self):
		self.controller.listenerSave()

	"""
		Méthode qui signale le début d'une restauration
	"""
	def load_file(self):
		self.controller.listenerLoad()
		
	"""
		Méthode qui signale le début d'une exportation
	"""
	def export_file(self):
		self.controller.listenerExport()

	"""
		Méthode qui signale au contoller de mute/un-mute la piste et qui met à jour le bouton
	"""
	def clickedSoundButton(self):
		if self.soundButtonState:
			soundOff=tkinter.PhotoImage(file=os.path.join(os.path.abspath(os.path.dirname("..\Images\pause.png")), "pause.png"),master=self.topPresentation)
			self.soundButton.config(image=soundOff)
			self.soundButton.image = soundOff
			self.soundButtonState = False
		else:
			soundOn=tkinter.PhotoImage(file=os.path.join(os.path.abspath(os.path.dirname("..\Images\play.png")), "play.png"),master=self.topPresentation)
			self.soundButton.config(image=soundOn)
			self.soundButton.image = soundOn
			self.soundButtonState = True
		self.controller.listenerPlay(self.soundButtonState)