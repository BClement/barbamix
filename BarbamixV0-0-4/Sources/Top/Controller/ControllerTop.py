# -*- coding: utf-8 -*-

"""
Cree le mercredi 10/04/2020

@author: L'Equipe Rocco // Corentin BRIAND
"""

import os

from AbstractTop import AbstractTop
from ControllerPiste import ControllerPiste
from PresentationTop import PresentationTop

##from ..Abstract.AbstractTop import AbstractTop
##from ...Pistes.Controller.ControllerPiste import ControllerPiste
##from ..Presentation.PresentationTop import PresentationTop

class ControllerTop():
    """
    """
    def __init__(self,topPresentation):
        """
        Constructeur du controller global
        
        
        Parametre
        ----------
        topPresentation : Master
            Zone d'affichage des presentations
        """
        self.master = topPresentation
        self.topPresentation = topPresentation
        self.controllerPiste = ControllerPiste(self.master)
        self.prensentationTop = PresentationTop(self, self.topPresentation) #TODO : verif constructeur
        self.abstractTop = AbstractTop()
        
        
    def addPiste(self):
        """
        Ajoute d'une piste a la table de mixage
            
        """
        self.controllerPiste.addPistes(1)
                
#=====================================================================================
    
    def listenerVolumeGeneral(self,Volume):
        self.abstractTop.setVolumeGeneral(Volume)
        self.controllerPiste.listenerVolumeGlobal(Volume)
		
    def listenerPlay(self,State):
        temp=0
        temp+=1
		#Je fait rien 
	
    def listenerSave(self):
        print("sauvegarde")
        #Je fait rien 
        
    def listenerLoad(self):
        print("charger")
        #Je fait rien 
        
    def listenerExport(self):
        print("exporter")
        #Je fait rien 
        
    def listenerMute(self,Mute):
        self.abstractTop.setMute(Mute)